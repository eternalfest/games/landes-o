const ANCHORS: ReadonlyMap<string, [number, number]> = new Map([
	["pf_bubble7_bout", [5, 0]],
	["pf_multi_bout", [5, 0]],
	["pf_motif_carre7_bout", [5, 0]],
	["pf_motif_ligne4_bout", [5, 0]],
	["pf_vigne_bout", [5, 0]],
	["pf_cristal_bout", [5, 0]],
	["pf_ninja_bout", [5, 0]],
	["spec_ninja", [13, 25]],
	["spec_perle", [10.5, 19.6]],
	["spec_poisson", [22.4, 28.1]],
	["spec_anneau", [12.5, 26]],
	["spec_jugement", [20.25, 32]],
	["spec_malediction", [21.3, 18]],
	["spec_costume", [12.4, 26.6]],
	["spec_medaillon", [12.5, 25]],
	["spec_sakura", [14.3, 29]],
	["spec_cadeau", [15, 28]],
	["spec_missingno", [15, 30]],
	["sfx_sakura", [50,50]],
	["score_acier", [10,28]],
	["score_combat", [10,28]],
	["score_dragon", [10,28]],
	["score_eau", [10,28]],
	["score_electricite", [10,28]],
	["score_fee", [10,28]],
	["score_feu", [10,28]],
	["score_obscurite", [10,28]],
	["score_plante", [10,28]],
	["score_psy", [10,28]],
	["$delon", [-5,15]],
	["$deloff", [-5,15]],
	["score_cb", [12.25,24]],
	["sfx_versus", [150,0]],
]);

const EXTRACT_FRAMES: ReadonlyMap<string, FrameRef> = new Map([

]);

const CLONE_SPRITES: ReadonlyMap<string, SpriteRef> = new Map([
	["boss_big_o", "34xqv"],
]);
const EXTRA_FRAMES: ReadonlyMap<SpriteRef, ExtraFrameOptions> = new Map([
	[
		"0S4B/=S7A",
		{
		  frames: [
			"pf_bubble7_corps",
			"pf_multi_corps",
			"pf_motif_carre7_corps",
			"pf_motif_ligne4_corps",
			"pf_vigne_corps",
			"pf_cristal_corps",
			"pf_ninja_corps"
		  ],
		},
	],
	[
		"]xUh6",
		{
		  frames: [
			"score_acier",
			"score_combat",
			"score_dragon",
			"score_eau",
			"score_electricite",
			"score_fee",
			"score_feu",
			"score_obscurite",
			"score_plante",
			"score_psy",
			"score_cb",
			],
		},
	],
	[
		"9-DjM",
		{
		  frames: [
			"spec_ninja",
			"spec_perle",
			"spec_poisson",
			"spec_anneau",
			"spec_jugement",
			"spec_malediction",
			"spec_costume",
			"spec_medaillon",
			"spec_sakura",
			"spec_missingno",
			"spec_cadeau",
			],
		},
	],
	[
		"0S4B/ nbpb",
		{
		  protectedDepths: new Set([1]), // shadow
		  frames: [
			"pf_bubble7_bout",
			"pf_multi_bout",
			"pf_motif_carre7_bout",
			"pf_motif_ligne4_bout",
			"pf_vigne_bout",
			"pf_cristal_bout",
			"pf_ninja_bout"
		  ],
		},
	],
	[
		"[Of51(",
		{
		  frames: [
			"fond_multi",
		  ],
		},
	],
	[
		"5Jij5(",
		{
		  frames: [
			"sfx_ninja",
			"sfx_jugement",
			"sfx_costume",
			"sfx_medaillon",
			"sfx_sakura_fond",
			"sfx_gris",
			"sfx_noir",
			"sfx_blanc",
			"sfx_rouge",
			"sfx_jaune",
			"sfx_vert",
			"sfx_bleu",
		  ],
		},
	]
]);

////////////////////////////////////////////////////////////////////////////////

import { PlaceObject } from "swf-types/tags";
import { getGamePath } from "@eternalfest/game";
import fs from "fs";
import sysPath from "path";
import { emitSwf } from "swf-emitter";
import { DynMovie, swfMerge } from "swf-merge";
import { parseSwf } from "swf-parser";
import { CompressionMethod, Matrix, Movie, NamedId, Sfixed16P16, SpriteTag, Tag, TagType } from "swf-types";
import { DefineSprite, DefineSprite as DefineSpriteTag } from "swf-types/tags/define-sprite";
import { BlendMode } from "swf-types/blend-mode";
import { Sfixed8P8 } from "swf-types/fixed-point/sfixed8p8";

const PROJECT_ROOT: string = sysPath.join(__dirname, "..");

const INPUT_GAME: string = getGamePath();
const INPUT_ASSETS: string = sysPath.join(PROJECT_ROOT, "build", "assets.swf");
const OUTPUT_GAME: string = sysPath.join(PROJECT_ROOT, "build", "game.swf");

interface ExtraFrameOptions {
  readonly protectedDepths?: ReadonlySet<number>,
  readonly frames: ReadonlyArray<string>,
}

const PIXELS_PER_TWIP: number = 20;

type SpriteRef = string | number;

type FrameRef = [SpriteRef, number];

async function main() {
  const game: Movie = await readSwf(INPUT_GAME);
  let assets: Movie = await readSwf(INPUT_ASSETS);
  assets = applyAnchors(assets, ANCHORS);
  // await writeFile("assets.json", JSON.stringify(assets, null, 2) as any);
  let merged: Movie = swfMerge(game, assets);
  merged = extractFrames(merged, EXTRACT_FRAMES);
  merged = cloneSprites(merged, CLONE_SPRITES);
  merged = appendFrames(merged, EXTRA_FRAMES);
  // await writeFile("game.json", JSON.stringify(merged, null, 2) as any);
  await writeSwf(OUTPUT_GAME, merged);
}

const IDENTITY_MATRIX: Matrix = {
  scaleX: Sfixed16P16.fromValue(1),
  scaleY: Sfixed16P16.fromValue(1),
  rotateSkew0: Sfixed16P16.fromValue(0),
  rotateSkew1: Sfixed16P16.fromValue(0),
  translateX: 0,
  translateY: 0,
};

function applyAnchors(astMovie: Movie, anchors: ReadonlyMap<string, [number, number]>): Movie {
  const movie = DynMovie.fromAst(astMovie);
  const exports = movie.getNamedExports();
  const definitions = movie.getDefinitions();
  const tags: Tag[] = [...astMovie.tags];
  for (const [linkageName, [posX, posY]] of anchors) {
    const id = exports.get(linkageName);
    if (id === undefined) {
      throw new Error(`ExportNotFound: linkageName = ${JSON.stringify(linkageName)}`);
    }
    const index = definitions.get(id);
    if (index === undefined) {
      throw new Error(`DefinitionNotFound: id = ${id}`);
    }
    const tag: Tag = tags[index];
    if (tag.type !== TagType.DefineSprite) {
      throw new Error(`UnexpectTagType: expected = ${TagType.DefineSprite} (DefineSprite), actual = ${tag.type} (${TagType[tag.type]})`);
    }
    const childTags: ReadonlyArray<SpriteTag> = tag.tags.map((childTag: SpriteTag) => {
      if (childTag.type !== TagType.PlaceObject || childTag.isUpdate) {
        return childTag;
      }
      const oldMatrix: Matrix = childTag.matrix !== undefined ? childTag.matrix : IDENTITY_MATRIX;
      const matrix = {
        ...oldMatrix,
        translateX: oldMatrix.translateX - (posX * PIXELS_PER_TWIP),
        translateY: oldMatrix.translateY - (posY * PIXELS_PER_TWIP),
      };
      return {...childTag, matrix};
    });
    tags[index] = {...tag, tags: childTags};
  }
  return {...astMovie, tags};
}

function cloneSprites(astMovie: Movie, sprites: ReadonlyMap<string, SpriteRef>): Movie {
  const movie = DynMovie.fromAst(astMovie);
  let nextId: number = movie.getMaxCharacterId() + 1;
  const tags: Tag[] = [...astMovie.tags];
  const extraExports: NamedId[] = [];
  for (const [cloneLinkName, spriteRef] of sprites) {
    const {index, sprite} = resolveSpriteRef(tags, movie, spriteRef);
    const clone: DefineSprite = {...sprite, id: nextId};
    tags.push(clone);
    extraExports.push({id: clone.id, name: cloneLinkName});
    nextId += 1;
  }
  tags.push({type: TagType.ExportAssets, assets: extraExports});
  return {...astMovie, tags};
}

function extractFrames(astMovie: Movie, anchors: ReadonlyMap<string, FrameRef>): Movie {
  const movie = DynMovie.fromAst(astMovie);
  let nextId: number = movie.getMaxCharacterId() + 1;
  const tags: Tag[] = [...astMovie.tags];
  const extraExports: NamedId[] = [];
  for (const [linkName, [spriteRef, frameIndex]] of anchors) {
    const {index, sprite} = resolveSpriteRef(tags, movie, spriteRef);
    const frameSprite: DefineSprite = extractFrame(sprite, frameIndex, nextId);
    tags.push(frameSprite);
    extraExports.push({id: frameSprite.id, name: linkName});
    nextId += 1;
  }
  tags.push({type: TagType.ExportAssets, assets: extraExports});
  return {...astMovie, tags};
  function extractFrame(sprite: DefineSprite, frameIndex: number, id: number): DefineSprite {
    let curFrame: number = 0;
    let curTagIndex: number = 0;
    const depths: Map<number, PlaceObject> = new Map();
    for (const tag of sprite.tags) {
      if (curFrame === frameIndex) {
        // We reached the frame we wanted to extract
        const spriteTags: SpriteTag[] = [];
        for (const object of depths.values()) {
          spriteTags.push(object);
        }
        for (let i: number = curTagIndex; i < sprite.tags.length; i++) {
          const spriteTag: SpriteTag = sprite.tags[i];
          if (spriteTag.type === TagType.ShowFrame) {
            break;
          }
          spriteTags.push(spriteTag);
        }
        spriteTags.push({type: TagType.ShowFrame});
        return {type: TagType.DefineSprite, id, frameCount: 1, tags: spriteTags};
      }

      if (tag.type === TagType.ShowFrame) {
        curFrame += 1;
      } else if (tag.type === TagType.PlaceObject) {
        if (!tag.isUpdate) {
          depths.set(tag.depth, tag);
        } else {
          let old: PlaceObject | undefined = depths.get(tag.depth);
          if (old === undefined) {
            old = {
              type: TagType.PlaceObject,
              isUpdate: false,
              depth: tag.depth,
              characterId: tag.characterId,
              className: tag.className,
              matrix: {
                scaleX: Sfixed16P16.fromValue(1),
                scaleY: Sfixed16P16.fromValue(1),
                rotateSkew0: Sfixed16P16.fromValue(0),
                rotateSkew1: Sfixed16P16.fromValue(0),
                translateX: 0,
                translateY: 0,
              },
              colorTransform: {
                redMult: Sfixed8P8.fromValue(1),
                greenMult: Sfixed8P8.fromValue(1),
                blueMult: Sfixed8P8.fromValue(1),
                alphaMult: Sfixed8P8.fromValue(1),
                redAdd: 0,
                greenAdd: 0,
                blueAdd: 0,
                alphaAdd: 0,
              },
              ratio: 0,
              name: tag.name,
              clipDepth: tag.clipDepth,
              filters: undefined,
              blendMode: BlendMode.Normal,
              bitmapCache: undefined,
              visible: undefined,
              backgroundColor: undefined,
              clipActions: undefined,
            };
          }
          const newTag: PlaceObject = {
            ...old,
            isUpdate: false,
            characterId: tag.characterId !== undefined ? tag.characterId : old.characterId,
            className: tag.className !== undefined ? tag.className : old.className,
            matrix: tag.matrix !== undefined ? tag.matrix : old.matrix,
            colorTransform: tag.colorTransform !== undefined ? tag.colorTransform : old.colorTransform,
            ratio: tag.ratio !== undefined ? tag.ratio : old.ratio,
            name: tag.name !== undefined ? tag.name : old.name,
            clipDepth: tag.clipDepth !== undefined ? tag.clipDepth : old.clipDepth,
            filters: tag.filters !== undefined ? tag.filters : old.filters,
            blendMode: tag.blendMode !== undefined ? tag.blendMode : old.blendMode,
            bitmapCache: tag.bitmapCache !== undefined ? tag.bitmapCache : old.bitmapCache,
            visible: tag.visible !== undefined ? tag.bitmapCache : old.visible,
            backgroundColor: tag.backgroundColor !== undefined ? tag.backgroundColor : old.backgroundColor,
            clipActions: tag.clipActions !== undefined ? tag.clipActions : old.clipActions,
          };
          depths.set(tag.depth, newTag);
        }
      } else if (tag.type === TagType.RemoveObject) {
        depths.delete(tag.depth);
      }

      curTagIndex += 1;
    }
    throw new Error("FrameNotFound: " + frameIndex);
  }
}

function appendFrames(astMovie: Movie, extraFrames: ReadonlyMap<SpriteRef, ExtraFrameOptions>) {
  const movie = DynMovie.fromAst(astMovie);
  const exports = movie.getNamedExports();
  const tags: Tag[] = [...astMovie.tags];
  for (const [extendedLinkageName, options] of extraFrames) {
    const protectedDepths: ReadonlySet<number> = options.protectedDepths !== undefined
      ? options.protectedDepths
      : new Set();
    const {index, sprite} = resolveSpriteRef(tags, movie, extendedLinkageName);
    tags[index] = appendFramesToSprite(sprite, options.frames, extendedLinkageName, protectedDepths);
  }
  return {...astMovie, tags};

  function appendFramesToSprite(
    tag: DefineSpriteTag,
    frames: ReadonlyArray<string>,
    extendedLinkageName: SpriteRef,
    protectedDepths: ReadonlySet<number>,
  ): DefineSpriteTag {
    let frameCount: number = tag.frameCount;
    const childTags: SpriteTag[] = [...tag.tags];
    for (const depth of getUsedDepths(childTags)) {
      if (protectedDepths.has(depth)) {
        continue;
      }
      childTags.push({type: TagType.RemoveObject, depth});
    }
    let minDepth = 0;
    for (const depth of protectedDepths) {
      minDepth = Math.max(minDepth, depth);
    }
    const depth = minDepth + 1;
    let first = true;
    for (const frameLinkageName of frames) {
      const id = exports.get(frameLinkageName);
      if (id === undefined) {
        throw new Error(`ExportNotFound: linkageName = ${JSON.stringify(frameLinkageName)}`);
      }
      console.log(`${extendedLinkageName}[${frameCount}] = ${frameLinkageName}`);
      if (first) {
        first = false;
      } else {
        childTags.push({type: TagType.RemoveObject, depth});
      }
      childTags.push(
        {
          type: TagType.PlaceObject,
          isUpdate: false,
          depth,
          characterId: id,
          className: undefined,
          matrix: undefined,
          colorTransform: undefined,
          ratio: undefined,
          name: undefined,
          clipDepth: undefined,
          filters: undefined,
          blendMode: undefined,
          bitmapCache: undefined,
          visible: undefined,
          backgroundColor: undefined,
          clipActions: undefined,
        },
        {type: TagType.ShowFrame},
      );
      frameCount += 1;
    }

    return {...tag, frameCount, tags: childTags};
  }
}

function getUsedDepths(tags: Iterable<Tag>): Set<number> {
  const depths: Set<number> = new Set();
  for (const tag of tags) {
    if (tag.type === TagType.PlaceObject) {
      depths.add(tag.depth);
    } else if (tag.type === TagType.RemoveObject) {
      depths.delete(tag.depth);
    }
  }
  return depths;
}

function resolveSpriteRef(tags: readonly Tag[], movie: DynMovie, ref: SpriteRef): {index: number, sprite: DefineSprite} {
  const exports = movie.getNamedExports();
  const definitions = movie.getDefinitions();
  let resolvedId: number
  if (typeof ref === "number") {
    resolvedId = ref;
  } else {
    const refParts: readonly string[] = typeof ref === "string" ? ref.split("/") : [];
    const spriteName: string = refParts[0];
    const objectName: string | undefined = 1 < refParts.length ? refParts[1] : undefined;
    const spriteId: number | undefined = exports.get(spriteName);
    if (spriteId === undefined) {
      throw new Error(`ExportNotFound: linkageName = ${JSON.stringify(spriteName)}`);
    }
    if (objectName === undefined) {
      resolvedId = spriteId;
    } else {
      const spriteIndex = definitions.get(spriteId);
      if (spriteIndex === undefined) {
        throw new Error(`DefinitionNotFound: id = ${spriteId}`);
      }
      const tag: Tag = tags[spriteIndex];
      if (tag.type !== TagType.DefineSprite) {
        throw new Error(`UnexpectTagType: expected = ${TagType.DefineSprite} (DefineSprite), actual = ${tag.type} (${TagType[tag.type]})`);
      }
      let subSpriteId: number | undefined = undefined;
      for (const childTag of tag.tags) {
        if (childTag.type === TagType.PlaceObject && childTag.name === objectName) {
          subSpriteId = childTag.characterId;
        }
      }
      if (subSpriteId === undefined) {
        throw new Error(`CharacterIdNotFound: extendedLinkageName = ${JSON.stringify(ref)}`);
      }
      resolvedId = subSpriteId;
    }
  }
  const index: number | undefined = definitions.get(resolvedId);
  if (index === undefined) {
    throw new Error(`DefinitionNotFound: id = ${resolvedId}`);
  }
  const spriteTag: Tag = tags[index];
  if (spriteTag.type !== TagType.DefineSprite) {
    throw new Error(`UnexpectTagType: expected = ${TagType.DefineSprite} (DefineSprite), actual = ${spriteTag.type} (${TagType[spriteTag.type]})`);
  }
  return {index, sprite: spriteTag};
}

async function readSwf(filePath: string): Promise<Movie> {
  const buffer: Buffer = await readFile(filePath);
  return parseSwf(buffer);
}

async function readFile(filePath: string): Promise<Buffer> {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, (err, data) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

async function writeSwf(filePath: string, movie: Movie): Promise<void> {
  const bytes: Uint8Array = emitSwf(movie, CompressionMethod.Deflate);
  return writeFile(filePath, bytes);
}

async function writeFile(filePath: string, data: Uint8Array): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    fs.writeFile(filePath, data, (err: NodeJS.ErrnoException | null) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

main();
