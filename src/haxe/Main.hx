import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import patchman.HostModuleLoader;
import patchman.IPatch;
import patchman.Patchman;

import quete.Quetes;
import landes.Landes;
import ice.Ice;

import better_script.BetterScript;
import vault.ISpec;

import atlas.Atlas;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    debug: Debug,
	ice : Ice,
	quete : Quetes,
	better_script : BetterScript,
	landes : Landes,
	darknessLevels: atlas.props.Darkness,
    ninjutsu: atlas.props.Ninjutsu,
	atlas : atlas.Atlas,
	vault: vault.Vault,
    merlin: Merlin,
    patches: Array<IPatch>,
    hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
