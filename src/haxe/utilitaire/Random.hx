package utilitaire;

class Random{
	// Classe de génération nombres pseudo-aléatoires d'AlphaBounce
	// Le générateur initialise la séquence

	public var vals : Array<Int> = [];
    public var idx : Null<Int> = 0;
    public var prec : Null<Int> = 0;
    
    public function new(arg0 : Null<Int>){
        this.initSeed(arg0);
	}
	
	public function int() : Int{
		this.idx = (this.idx + 1) % this.prec;
        this.vals[this.idx] = this.vals[(this.idx + 24) % this.prec] + this.idx & 1073741823;
        return this.vals[this.idx];
	}
	
	public function rand() : Float{
		return (this.int() % 1000) / 1000.0;
	}
	
	public function random(modulo : Int) : Int{
		return this.int() % modulo;
	}
	public function initSeed(arg0 : Null<Int>) : Void{
		if (arg0 == null)
            arg0 = 133;
        this.prec = 55;
        this.idx = 0;
        this.vals = [];
        for (i in 0...this.prec){
            arg0 = (arg0 * 1103515245) & 1073741823;
            arg0 = arg0 +  12345;
            arg0 = arg0 & 1073741823;
            var truc = arg0 & 1073676288;
            arg0 = (arg0 * 1103515245) & 1073741823;
            arg0 = arg0 +  12345;
            arg0 = arg0 & 1073741823;
            this.vals.push(arg0 >> 16 | truc);
		}
	}
}