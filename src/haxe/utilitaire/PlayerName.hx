package utilitaire;

import patchman.IPatch;
import patchman.Ref;
import etwin.ds.FrozenArray;
import hf.Hf;
import etwin.Obfu;
import patchman.HostModuleLoader;
import patchman.module.Run;
import hf.Lang;

@:build(patchman.Build.di())
class PlayerName {

    public var run : patchman.module.Run;
    
    @:diExport
    public var playerName(default, null): IPatch;

    public function new(jeu: patchman.module.Run) {
        run = jeu;
        this.playerName = Ref
        .auto(hf.Lang.get)
        .postfix(function(hf, id : Int, resultat){
            return StringTools.replace(resultat,"{playerName}",run.getRun().user.displayName);
        });
    }
}