package utilitaire;

import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class FixedBackground implements IAction {
  public var name(default, null): String;
  public var isVerbose(default, null): Bool = false;
  public var background: Dynamic;

  public function new(name: String) {
    this.name = name;
  }

  public function run(ctx: IActionContext): Bool {
      var game: GameMode = ctx.getGame();

    var id: Int = ctx.getInt(Obfu.raw("id"));
    background.removeMovieClip();
    background = cast game.depthMan.attach("hammer_bg", game.root.Data.DP_BACK_LAYER - 15);
    background.gotoAndStop(Std.string(id));
    return false;
  }
}
