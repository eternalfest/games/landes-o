package landes;

import hf.entity.Shoot;
import hf.Entity;
import hf.mode.GameMode;
import hf.Data;
import hf.Hf;

import etwin.Obfu;

@:hfTemplate
class PepinBig_O extends hf.entity.Shoot {
  
  public function new(hf : Hf): Void {
    super(null);
    this.shootSpeed = 5;
    this._yOffset = -15;
	//this.rotation = Std.random(360);
  }

  public static function attach(g: GameMode, x: Float, y: Float): PepinBig_O {
	g.root.Std.registerClass(Obfu.raw("boss_big_o_pepin"), PepinBig_O.getClass(g.root));
    var pepin: PepinBig_O = cast g.depthMan.attach(Obfu.raw("boss_big_o_pepin"), g.root.Data.DP_SHOTS);
		pepin.initShoot(g, x, y);
    return pepin;
  }

  override public function hit(e: Entity): Void {
    if ((e.types & e.game.root.Data.PLAYER) > 0) {
      var v3: hf.entity.Player = cast e;
      v3.killHit(this.dx);
    }
  }
}
