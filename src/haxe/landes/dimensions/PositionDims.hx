package landes.dimensions;

import patchman.module.Run;
import utilitaire.Random;
import etwin.Obfu;
import etwin.ds.WeakMap;
import patchman.IPatch;
import patchman.Ref;
import patchman.PatchList;

import hf.Hf;
import hf.mode.GameMode;
import hf.Lang;
import hf.entity.Player;
import hf.Entity;
import hf.levels.PortalLink;
import hf.Data;

@:build(patchman.Build.di())
class PositionDims {

	public var run : patchman.module.Run;
	public var idString : String;
	public var idInt : Int;
	public var seed : Random;
	public var niveaux : Map<Int, String>;
	public var leviers : Map<Int, String>;
	public var leviersActives : Map<Int, Bool>;
	public var sauvegarde : Int;
	public var initialisation : Bool = false;
	
	public static var NIVEAU_MIN : Int = 5;
	public static var NIVEAU_MAX : Int = 25;
	public static var NIVEAU_EI_MAX : Int = 12;
	
	public static var LEVIERS_MAX : Int = 10;
	
	public static var L_1 = [Obfu.raw("ninja"), Obfu.raw("ice"), Obfu.raw("cauchemar"), Obfu.raw("miroir")];
	public static var L_2 = [Obfu.raw("miroir"), Obfu.raw("ninja"), Obfu.raw("ice"), Obfu.raw("cauchemar"),  Obfu.raw("ei")];

	public function convertion(id : String) : Int{
		return Std.parseInt('0x' + id.substr(0,8));
	}
	
	@:diExport
	public var PATCH_GESTION_DIMS(default, null): IPatch;
	
	public static function tirerNiveaux(seed : Random) : Map<Int, String>{
		var map = new Map<Int, String>();
			map.set(seed.random(PositionDims.NIVEAU_EI_MAX - PositionDims.NIVEAU_MIN) + PositionDims.NIVEAU_MIN, Obfu.raw("ei"));
			
		for (type in PositionDims.L_1){
			var niveau = seed.random(PositionDims.NIVEAU_MAX - PositionDims.NIVEAU_MIN) + PositionDims.NIVEAU_MIN;
			while (map.exists(niveau)){
				niveau = seed.random(PositionDims.NIVEAU_MAX - PositionDims.NIVEAU_MIN) + PositionDims.NIVEAU_MIN;
			}
			map.set(niveau, type);
		}
		return map;
	}

	public static function tirerLeviers(seed : Random) : Map<Int, String>{
		var map = new Map<Int, String>();
		for (type in PositionDims.L_2){
			var position = seed.random(PositionDims.LEVIERS_MAX);
			while (map.exists(position)){
				position = seed.random(PositionDims.LEVIERS_MAX);
			}
			map.set(position, type);
		}
		return map;
	}

	public function new(run: patchman.module.Run) {
		PATCH_GESTION_DIMS =
			Ref.auto(GameMode.initGame)
			.after(function(hf: Hf,self : GameMode) : Void{
				if(initialisation)
					return;
				else{
					initialisation = true;
				}
				var inv = [for (key in niveaux.keys()) niveaux[key] => key];
				for (type in  L_2){
					var pos = hf.Data.getLevelFromTag(type+"+0");
					var lidFin = pos.lid;
					while (self.dimensions[1].raw[lidFin].length > 500)
					{
						lidFin += 1;
					}
					if(pos != null && lidFin != null){
						var portail = hf.levels.PortalLink._new();
							portail.from_did 	= 0;
							portail.from_lid 	= inv.get(type);
							portail.from_pid 	= 777;
							portail.to_did 		= pos.did;
							portail.to_lid 		= pos.lid;
							portail.to_pid 		= -1;
						hf.Data.LINKS.push(portail);
							portail 			= hf.levels.PortalLink._new();
							portail.from_did 	= 1;
							portail.from_lid 	= lidFin - 1;
							portail.from_pid 	= 0;
							portail.to_did 		= 0;
							portail.to_lid 		= inv.get(type)+1;
							portail.to_pid 		= -1;
						hf.Data.LINKS.push(portail);
						//patchman.DebugConsole.log(lidFin - pos.lid);
					}else{
						//patchman.DebugConsole.log(type);
					}
				}
		});
		this.run = run;
        this.idString = this.run.getRun().user.id;
		this.idInt = convertion(this.idString);
		this.seed = new Random(this.idInt);
		this.niveaux = tirerNiveaux(this.seed);
		this.leviers = tirerLeviers(this.seed);
		this.leviersActives = [for(key in 0...10) key => false];
		this.sauvegarde = 0;
    }
}