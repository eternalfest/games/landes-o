package landes.spec;

import hf.mode.GameMode;
import hf.entity.Item;
import vault.ISpec;

class SpecPoisson implements ISpec {

	public var id(default, null): Int = 120;
	public static var STATIC_MESSAGE = 604;
	
	public function new() {}

	public function execute(hf: hf.Hf, specMan: hf.SpecialManager, item : Item): Void {
			specMan.game.endLevelStack.push(function () {
				for (v2 in 0...15) {
					hf.entity.item.ScoreItem.attach(specMan.game, Std.random(hf.Data.GAME_WIDTH), -30 - Std.random(50), Std.random(5) + 1101, null);
				}
			});
			// Affiche le texte
			// specMan.player.game.attachPop("\n" + hf.Lang.get(STATIC_MESSAGE), false);
			
			// Supprime la flèche
			specMan.player.game.killPointer();
	}

	public function interrupt(hf: hf.Hf, specMan: hf.SpecialManager): Void{}

}
