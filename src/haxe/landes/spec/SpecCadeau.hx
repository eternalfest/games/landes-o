package landes.spec;

import hf.mode.GameMode;
import hf.entity.Item;
import vault.ISpec;

class SpecCadeau implements ISpec {

	public var id(default, null): Int = 128;

	public function new() {}

	public function execute(hf: hf.Hf, specMan: hf.SpecialManager, item : Item): Void {
		var alea = Std.random(117);
		while ((alea >= 40 && alea <= 63)||(alea == 0)){
			alea = Std.random(117);
		}
		item.id = alea;
		specMan.execute(cast item);
	}

	public function interrupt(hf: hf.Hf, specMan: hf.SpecialManager): Void{}

}
