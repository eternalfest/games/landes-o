package landes.spec;

import patchman.IPatch;
import patchman.Ref;
import etwin.Obfu;
import patchman.HostModuleLoader;

import hf.mode.GameMode;
import hf.entity.Item;
import hf.entity.Player;
import hf.entity.Bomb;
import hf.Entity;
import hf.Pos;
import hf.Hf;

import quete.Quetes;

import etwin.Obfu;

import vault.ISpec;

import landes.spec.SfxSakura;

class SpecSakura implements ISpec {

	public var id(default, null): Int = 126;
	public static var BG_POKEMON = 18;
	public var quete : Quetes;
	public static var STATIC_MESSAGE = 652;
	
	public function new(quete : Quetes) {
		this.quete = quete;
	}

	public function execute(hf: hf.Hf, specMan: hf.SpecialManager, item : Item): Void {
		specMan.player.game.attachPop("\n" + hf.Lang.get(STATIC_MESSAGE), false);
		// Affiche le SFX cercle.
		var sakura = new SfxSakura(specMan.game);
		// Affiche le fond
		specMan.game.fxMan.attachBg(SpecSakura.BG_POKEMON, null, 99999);
		// Compte le nombre de cartes obtenues.
		var objetsQuetes = this.quete.donneObjets();// Préalablement
		var objetsJeu = specMan.game.scorePicks;// Dans cette partie
		var L = [];
		var min = Math.POSITIVE_INFINITY;
		for (i in 239...249){
			var nb_quete : Float = objetsQuetes[i + 1000];
			if (nb_quete == null)
				nb_quete = 0;
			var nb_jeu : Float  = objetsJeu[i];
			if (nb_jeu == null)
				nb_jeu = 0;
			var n = nb_jeu + nb_quete;
			if (n < min){
				min = n;
				L = [i];
			}
			else if(n == min){
				L.push(i);
			}
		}
		if(L.length > 0){
			/// Fait apparaître une carte aléatoire parmi les cartes obtenues le moins de fois.
			var objet = hf.entity.item.ScoreItem.attach(specMan.game, 205, 250, L[Std.random(L.length)], null);
            objet.setLifeTimer(hf.Data.SECOND * 3);
		}
	}

	public function interrupt(hf: hf.Hf, specMan: hf.SpecialManager): Void{
	}

}
