package landes.spec;

import patchman.IPatch;
import patchman.Ref;

import hf.mode.GameMode;
import hf.entity.Item;
import hf.entity.Player;
import hf.entity.Bomb;
import hf.Entity;
import hf.Pos;
import hf.Hf;

import vault.ISpec;

class SpecCostume implements ISpec {

	public var id(default, null): Int = 124;
	public static var BG_DRAGONBALL = 16;
	public static var STATIC_MESSAGE = 605;

	public var PATCH_attack(default, null): IPatch;
	public var PATCH_kickBomb(default, null): IPatch;
	public function new() {
		PATCH_attack = Ref.auto(Player.attack)
		.after(function(hf: Hf, self: Player, bombe : Null<Entity>) {
			if (self.specialMan.actives[id]) {
				(cast bombe).upgradeBomb(self);
				(cast bombe).power *= 1.5;
				return bombe;
			}
			return bombe;
		});
		PATCH_kickBomb = Ref.auto(Player.kickBomb)
		.after(function(hf: Hf, self: Player, l: Array<Bomb>, powerFactor: Float) {
			if (self.specialMan.actives[id]) {
				for (bombe in l){
					bombe.dx *= 1.5;
				}
			}
		});
	}

	public function execute(hf: hf.Hf, specMan: hf.SpecialManager, item : Item): Void {
		specMan.game.fxMan.attachBg(SpecCostume.BG_DRAGONBALL, null, hf.Data.SECOND * 3);
		specMan.game.fxMan.attachFx(specMan.player.x - 45, specMan.player.y - 45 - specMan.player._height/2, "$puissance");
		specMan.player.scale(150);
		specMan.player.fallFactor = 1.6;
		
		specMan.permanent(id);
		
		// Affiche le texte
		//specMan.player.game.attachPop("\n" + hf.Lang.get(STATIC_MESSAGE), false);
			
		// Supprime la flèche
		specMan.player.game.killPointer();
	}

	public function interrupt(hf: hf.Hf, specMan: hf.SpecialManager): Void{
		specMan.actives[id] = false;
		specMan.game.fxMan.attachFx(specMan.player.x - 45, specMan.player.y - 45 - specMan.player._height/2, "$puissance");
		specMan.player.scale(100);
		specMan.player.fallFactor = 1.1;
	}

}
