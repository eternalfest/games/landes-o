package landes.spec;

import hf.mode.GameMode;
import hf.entity.Item;
import vault.ISpec;

class SpecMissingno implements ISpec {

	public var id(default, null): Int = 127;

	public function new() {}

	public function execute(hf: hf.Hf, specMan: hf.SpecialManager, item : Item): Void {
			specMan.player.minAlpha = 0;
			specMan.player.alpha = 0;
	}

	public function interrupt(hf: hf.Hf, specMan: hf.SpecialManager): Void{}

}
