package landes.spec;

import etwin.Obfu;
import patchman.IPatch;
import patchman.Ref;

import hf.Animation;
import hf.mode.GameMode;
import hf.Data;


@:hfTemplate
class SfxSakura extends Animation {

	public var etat = 0;

	public override function update(){
		if(etat == 0){
			this.mc._xscale += 1;
			this.mc._yscale += 1;
			if (this.mc._xscale > 300){
				etat = 1;
			}
		}
		else if (etat == 1){
			this.mc._rotation += 1;
			if( this.mc._rotation == 0){
				etat = 2;
			}
		}
		else if (etat == 2){
			this.mc._xscale += 1;
			this.mc._yscale += 1;
			if (this.mc._xscale > 600){
				etat = 3;
			}
		}
		else if (etat == 3){
			this.mc._alpha -= 1;
			if (this.mc._alpha <= 0){
				this.destroy();
			}
		}
	};

	public function new(g : GameMode){
		super(g.root,g);
		this.attach(210,250, Obfu.raw("sfx_sakura"), 11);
		g.fxMan.animList.push(this);
	};
}