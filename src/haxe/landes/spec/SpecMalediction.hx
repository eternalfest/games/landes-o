package landes.spec;

import patchman.IPatch;
import patchman.Ref;

import hf.mode.GameMode;
import hf.entity.Item;
import hf.entity.Player;
import hf.Pos;
import hf.Hf;

import vault.ISpec;

class SpecMalediction implements ISpec {

	public var id(default, null): Int = 123;
	public static var COULEUR_NEGATIF = 16711680;
	public static var STATIC_MESSAGE = 602;
	
	public var PATCH_getScore(default, null): IPatch;
	public var PATCH_getScoreHidden(default, null): IPatch;
	public var PATCH_aura(default, null): IPatch;
	
	public function new() {
			PATCH_getScore = Ref.auto(Player.getScore)
			.replace(function(hf: Hf,self : Player, origin: Pos<Float>, value: Int) : Void{
				if (origin != null) {
					var coeff = (self.specialMan.actives[95] ? 2 : 1);
						coeff *= (self.specialMan.actives[id] ? -1 : 1);
					var color = (coeff > 0 ? self.darkColor : SpecMalediction.COULEUR_NEGATIF);
					self.game.fxMan.attachScorePop(self.baseColor, color, origin.x, origin.y, "" + value * coeff);
				}
				self.getScoreHidden(value);
			});
			
			PATCH_getScoreHidden = Ref.auto(Player.getScoreHidden)
			.wrap(function(hf: Hf,self : Player, value: Int, old) : Void{
					var coeff = (self.specialMan.actives[id] ? -1 : 1);
					old(self,value * coeff);
			});
			
			PATCH_aura = Ref.auto(Player.update)
			.after(function(hf: Hf, self: Player) {
				if (self.specialMan.actives[id]) {
					var v3 = self.game.fxMan.attachFx(self.x + hf.Std.random(15) * (hf.Std.random(2) * 2 - 1) - 5, self.y - hf.Std.random(30) - self._height/2, "$aura");
					v3.mc._xscale = hf.Std.random(70) + 30;
					v3.mc._yscale = v3.mc._xscale;
				}
			});
	}

	public function execute(hf: hf.Hf, specMan: hf.SpecialManager, item : Item): Void {
		specMan.permanent(id);
		
		// Affiche le texte
		//specMan.player.game.attachPop("\n" + hf.Lang.get(STATIC_MESSAGE), false);
		
		// Supprime la flèche
		specMan.player.game.killPointer();
	}

	public function interrupt(hf: hf.Hf, specMan: hf.SpecialManager): Void{
		specMan.actives[id] = false;
	}

}
