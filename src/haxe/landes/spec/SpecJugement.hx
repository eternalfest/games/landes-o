package landes.spec;

import hf.mode.GameMode;
import hf.entity.Item;

import vault.ISpec;

class SpecJugement implements ISpec {

	public var id(default, null): Int = 122;
	public static var BG_JUGEMENT_ETERNEL = 15;
	public static var STATIC_MESSAGE = 600;

	public function new() {}

	public function execute(hf: hf.Hf, specMan: hf.SpecialManager, item : Item): Void {
		var nb_abricots = 0;
		// Tue les fruits
		var liste = specMan.game.getBadClearList();
		for (fruit in liste){
			if(Std.is(fruit, hf.entity.bad.walker.Abricot)){
				if ((cast fruit).fl_spawner)
					nb_abricots++;
			}
			specMan.game.fxMan.inGameParticles(hf.Data.PARTICLE_SPARK, fruit.x, fruit.y, Std.random(5) + 1);
			specMan.game.fxMan.attachFx(fruit.x, fruit.y, "hammer_fx_burning");
			fruit.dropReward();
			fruit.destroy();
		}
		
		// Affiche le fond
		specMan.game.fxMan.attachBg(SpecJugement.BG_JUGEMENT_ETERNEL, null, 9999);
		
		// Affiche le texte
		//specMan.player.game.attachPop("\n" + hf.Lang.get(STATIC_MESSAGE), false);
		
		// Crée le cristal géant
		for (joueur in specMan.game.getPlayerList()) {
			joueur.setBaseAnims(hf.Data.ANIM_PLAYER_WALK_V, hf.Data.ANIM_PLAYER_STOP_V);
		}
		hf.entity.supa.SupaItem.attach(specMan.game, (cast specMan.game).perfectCount - 1 + 2*nb_abricots);
		specMan.game.statsMan.inc(hf.Data.STAT_SUPAITEM, 1);
	}

	public function interrupt(hf: hf.Hf, specMan: hf.SpecialManager): Void{}

}
