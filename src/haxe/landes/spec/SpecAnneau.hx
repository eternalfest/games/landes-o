package landes.spec;

import etwin.ds.WeakMap;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;

import hf.mode.Adventure;
import hf.entity.Item;
import hf.Hf;
import vault.ISpec;

import atlas.PropBridge;
import atlas.Property;
import atlas.PropTypes;
import atlas.Atlas;

import etwin.Obfu;

class SpecAnneau implements ISpec {

	public var id(default, null): Int = 121;
	public static var STATIC_MESSAGE = 601;
	
	public static var ITEM_FORCE(default, null): Property<Int> = new Property();

	public function new() {
		PATCH = Ref.auto(Adventure.addLevelItems)
			.wrap(function(hf: Hf, self : Adventure, old) : Void {
				var identifiant : etwin.ds.Nil<Int> = Atlas.getProperty(self, ITEM_FORCE);
				if (self.world.current.__dollar__specialSlots.length > 0) {
					self.statsMan.spreadExtend();
				}
				if (self.world.current.__dollar__specialSlots.length > 0) {
					var IDslot = Std.random(self.world.current.__dollar__specialSlots.length);
					var slot = self.world.current.__dollar__specialSlots[IDslot];
					if (identifiant.isNone()){
						self.world.scriptEngine.insertSpecialItem(self.randMan.draw(hf.Data.RAND_ITEMS_ID), null, slot.__dollar__x, slot.__dollar__y, hf.Data.SPECIAL_ITEM_TIMER, null, false, true);
					}else{
						self.world.scriptEngine.insertSpecialItem(identifiant.toNullable(), null, slot.__dollar__x, slot.__dollar__y, hf.Data.SPECIAL_ITEM_TIMER, null, false, true);
					}
					if (self.globalActives[id])// ANNEAU ANTIK
					{
						var posX = Std.random(hf.Data.LEVEL_WIDTH);
						var posY = Std.random(hf.Data.LEVEL_HEIGHT - 5);
						var positionSol = self.world.getGround(posX, posY);
						self.world.scriptEngine.insertSpecialItem(self.randMan.draw(hf.Data.RAND_ITEMS_ID), null, positionSol.x, positionSol.y, hf.Data.SPECIAL_ITEM_TIMER, null, false, true);
					}
				}
				if (self.world.current.__dollar__scoreSlots.length > 0) {
					var IDslot = Std.random(self.world.current.__dollar__scoreSlots.length);
					var slot = self.world.current.__dollar__scoreSlots[IDslot];
					self.world.scriptEngine.insertScoreItem(self.randMan.draw(hf.Data.RAND_SCORES_ID), null, slot.__dollar__x, slot.__dollar__y, hf.Data.SCORE_ITEM_TIMER, null, false, true);
					if (self.globalActives[94]) {// ANNEAU ANTOK
						var v5 = Std.random(hf.Data.LEVEL_WIDTH);
						var v6 = Std.random(hf.Data.LEVEL_HEIGHT - 5);
						var v7 = self.world.getGround(v5, v6);
						self.world.scriptEngine.insertScoreItem(self.randMan.draw(hf.Data.RAND_SCORES_ID), null, v7.x, v7.y, hf.Data.SCORE_ITEM_TIMER, null, false, true);
					}
				}
			});
	}

	public var PATCH(default, null): IPatch;
			
	public function execute(hf: hf.Hf, specMan: hf.SpecialManager, item : Item): Void {
			specMan.global(id);
			specMan.permanent(id);
			
			// Affiche le texte
			//specMan.player.game.attachPop("\n" + hf.Lang.get(STATIC_MESSAGE), false);
			
			// Supprime la flèche
			specMan.player.game.killPointer();
		
	}
	public function interrupt(hf: hf.Hf, specMan: hf.SpecialManager): Void{
		specMan.game.globalActives[id] = false;
	}
}