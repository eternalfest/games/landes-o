package landes.spec;

import hf.mode.GameMode;
import hf.entity.Item;
import vault.ISpec;

class SpecPerle implements ISpec {

	public var id(default, null): Int = 119;
	public static var ID_PIECE = 51;
	public static var ID_DIAMANT = 17;
	public static var STATIC_MESSAGE = 603;

	public function new() {}

	public function execute(hf: hf.Hf, specMan: hf.SpecialManager, item : Item): Void {
		specMan.game.destroyList(hf.Data.BAD);
        specMan.game.destroyList(hf.Data.ITEM);
        specMan.game.destroyList(hf.Data.BAD_BOMB);
		hf.Data.CONVERT_DIAMANT = SpecPerle.ID_PIECE;
        specMan.levelConversion(hf.Data.CONVERT_DIAMANT, 2);
        specMan.temporary(id, hf.Data.SECOND * 19);
		
		// Affiche le texte
		// specMan.player.game.attachPop("\n" + hf.Lang.get(STATIC_MESSAGE), false);
		// Supprime la flèche
		specMan.player.game.killPointer();
	}

	public function interrupt(hf: hf.Hf, specMan: hf.SpecialManager): Void{
		specMan.game.destroyList(hf.Data.PERFECT_ITEM);
		hf.Data.CONVERT_DIAMANT = SpecPerle.ID_DIAMANT;
	}

}
