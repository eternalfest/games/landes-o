package landes.spec;

import patchman.IPatch;
import patchman.Ref;

import hf.mode.GameMode;
import hf.entity.Item;
import hf.entity.Player;
import hf.entity.Bomb;
import hf.Entity;
import hf.Pos;
import hf.Hf;

import vault.ISpec;

class SpecMedaillon implements ISpec {

	public var id(default, null): Int = 125;
	public static var BG_MOLTEAR = 17;
	public var PATCH_pause(default, null): IPatch;
	public static var STATIC_MESSAGE = 607;
	public function new() {
		PATCH_pause = Ref.auto(GameMode.main)
			.after(function(hf: Hf,self : GameMode) : Void{
				if (self.globalActives[id]){
					self.huTimer = 0;
				}
			});
	}

	public function execute(hf: hf.Hf, specMan: hf.SpecialManager, item : Item): Void {
		specMan.game.fxMan.attachBg(SpecMedaillon.BG_MOLTEAR, null, hf.Data.SECOND * 120);
		specMan.temporary(id, hf.Data.SECOND * 120);
		specMan.global(id);
		
		// Affiche le texte
		//specMan.player.game.attachPop("\n" + hf.Lang.get(STATIC_MESSAGE), false);
			
		// Supprime la flèche
		specMan.player.game.killPointer();
	}

	public function interrupt(hf: hf.Hf, specMan: hf.SpecialManager): Void{
		specMan.game.globalActives[id] = false;
		specMan.game.fxMan.clearBg();
	}

}
