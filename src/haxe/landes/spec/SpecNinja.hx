package landes.spec;

import hf.mode.GameMode;
import hf.entity.Item;
import vault.ISpec;

class SpecNinja implements ISpec {

	public var id(default, null): Int = 118;
	public static var BG_NINJA = 14;
	public static var STATIC_MESSAGE = 606;

	public function new() {}

	public function execute(hf: hf.Hf, specMan: hf.SpecialManager, item : Item): Void {
		var estFond = false;
		if (!specMan.game.fl_ninja){
			var fruits = specMan.game.getBadClearList();
			if (fruits.length > 1){
				estFond = true;
				fruits[0].fl_ninFoe = true;
				fruits[0].fl_ninFriend = false;
				fruits[1].fl_ninFoe = false;
				fruits[1].fl_ninFriend = true;
				for (i in 2...fruits.length){
					var T = (Std.random(3) != 0);
					fruits[i].fl_ninFriend = T;
					fruits[i].fl_ninFoe = false;
				}
			}
		}else{
			estFond = true;
			specMan.player.lives++;
			specMan.game.gi.setLives(specMan.player.pid, specMan.player.lives);
			specMan.game.fxMan.attachShine(item.x, item.y - hf.Data.CASE_HEIGHT * 0.5);
		}
		if (estFond){
			specMan.game.fxMan.attachBg(SpecNinja.BG_NINJA, null, hf.Data.SECOND * 3);
		}
		specMan.player.game.killPointer();
	}

	public function interrupt(hf: hf.Hf, specMan: hf.SpecialManager): Void{}

}
