package landes.actions;

import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

import color_matrix.ColorMatrix;

import hf.mode.GameMode;
import landes.dimensions.PositionDims;
import patchman.HostModuleLoader;
import ice.Ice;

class OpenColorPortalDim implements IAction {

	public var name(default, null): String = Obfu.raw("openColorPortalDim");
	public var isVerbose(default, null): Bool = false;
	public var positions : PositionDims;
	public var loader : HostModuleLoader;

	public function new(loader : HostModuleLoader, pos : PositionDims) {
		this.positions = pos;
		this.loader = loader;
	}
	
	private function peutOuvrir(niveau : Int, game : GameMode) : String{
		if (!positions.niveaux.exists(niveau)){
			return null;
		}
		else{
			if (game.fl_ninja && positions.niveaux.get(niveau) == Obfu.raw("ninja")){
				return "ninja";
			}
			if (game.fl_nightmare && positions.niveaux.get(niveau) == Obfu.raw("cauchemar")){
				return "cauchemar";
			}
			if (game.fl_bombExpert && positions.niveaux.get(niveau) == Obfu.raw("ei")){
				return "ei";
			}
			if (game.fl_mirror && positions.niveaux.get(niveau) == Obfu.raw("miroir")){
				return "miroir";
			}
			if (positions.niveaux.get(niveau) == Obfu.raw("ice") && Ice.isOptionEnabled(loader))
				return "ice";
			}
		return null;
	}

	public function run(ctx: IActionContext): Bool {
		var game = ctx.getGame();
		var niveau = game.dimensions[0].currentId;
		var typeOuverture = peutOuvrir(niveau, game);
		if (typeOuverture != null){
			var x: Null<Float> = ctx.getOptFloat(Obfu.raw("x")).toNullable();
			var y: Null<Float> = ctx.getOptFloat(Obfu.raw("y")).toNullable();
			//var pid: Null<Int> = ctx.getOptInt(Obfu.raw("pid"));
			var pid = 777;
			var angle = positions.seed.random(360);
			if (game.portalMcList[pid] != null) {
				return false;
			}
			game.world.scriptEngine.insertPortal(x, y, pid);
			var cx = game.root.Entity.x_ctr(game.flipCoordCase(x));
			var cy = game.root.Entity.y_ctr(y) - game.root.Data.CASE_HEIGHT * 0.5;
			var sprite = game.depthMan.attach("hammer_portal", game.root.Data.DP_SPRITE_BACK_LAYER);
				sprite._x = cx;
				sprite._y = cy;
				sprite.filters = [ColorMatrix.fromHsv(angle,1, 1).toFilter()];
			game.fxMan.attachExplosion(cx, cy, 30);
			game.fxMan.inGameParticles(game.root.Data.PARTICLE_PORTAL, cx, cy, 5);
			game.fxMan.attachShine(cx, cy);
			game.portalMcList[pid] = {x: cx, y: cy, mc: sprite, cpt: 0};
			this.positions.sauvegarde = niveau;
			return true;
		}else{
			return false;
		}
	}
}