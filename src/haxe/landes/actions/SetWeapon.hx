package landes.actions;

import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

/// Modifie la bombe d'Igor et Sandy.
/*
	WEAPON_B_CLASSIC: Int = 1;
	WEAPON_B_BLACK: Int = 2;
	WEAPON_B_BLUE: Int = 3;
	WEAPON_B_GREEN: Int = 4;
	WEAPON_B_RED: Int = 5;
	WEAPON_B_REPEL: Int = 9;
	WEAPON_NONE: Int = -1;
	WEAPON_S_ARROW: Int = 6;
	WEAPON_S_FIRE: Int = 7;
	WEAPON_S_ICE: Int = 8;
*/

class SetWeapon implements IAction {

	public var name(default, null): String = Obfu.raw("setWeapon");
	public var isVerbose(default, null): Bool = false;

	public function new() {}

	public function run(ctx: IActionContext): Bool {
		var game = ctx.getGame();
		var id: Null<Int> = ctx.getOptInt(Obfu.raw("id")).toNullable();
		for (joueur in game.getPlayerList()){
			joueur.changeWeapon(id);
		}
		return false;
	}
}