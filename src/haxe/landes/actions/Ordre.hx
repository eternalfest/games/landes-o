package landes.actions;

import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

import merlin.IAction;
import patchman.IPatch;
import patchman.Ref;
import etwin.ds.FrozenArray;

import hf.Hf;

import color_matrix.ColorMatrix;

class LevelOrdre implements IAction{

	public var name(default, null): String = Obfu.raw("levelOrdre");
	public var isVerbose(default, null): Bool = false;
	
	private var ordre : Ordre;

	public function new(ordre : Ordre) {
		this.ordre = ordre;
	}

	public function run(ctx: IActionContext): Bool {
		return ordre.ORDRE_FAIT;
	}
}

@:build(patchman.Build.di())
class Ordre{

	public var ORDRE_FAIT = false;
	
	@:diExport
	public var levelOrdre(default, null): IAction;
	
	@:diExport
	public var modificationNiveauEntree(default, null): IPatch;
	
	@:diExport
	public var modificationCristalGeant(default, null): IPatch;

	public function new() {
		this.modificationNiveauEntree = Ref
			.auto(hf.mode.GameMode.goto)
			.before(function(hf,self, id: Int): Void {
				ORDRE_FAIT = false;
		});
		this.modificationCristalGeant = Ref
			.auto(hf.entity.supa.SupaItem.initSupa)
			.before(function(hf,self,g: hf.mode.GameMode, x:Float, y:Float): Void {
				ORDRE_FAIT = true;
		});
		this.levelOrdre = new LevelOrdre(this);
	}
}