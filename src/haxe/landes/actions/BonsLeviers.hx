package landes.actions;

import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

import color_matrix.ColorMatrix;

import hf.mode.GameMode;
import landes.dimensions.PositionDims;
import patchman.HostModuleLoader;

class BonsLeviers implements IAction {

	public var name(default, null): String = Obfu.raw("bonsLeviers");
	public var isVerbose(default, null): Bool = false;
	public var positions : PositionDims;
	public var loader : HostModuleLoader;

	public function new(pos : PositionDims) {
		this.positions = pos;
	}

	public function run(ctx: IActionContext): Bool {
		var game = ctx.getGame();
		for (key in this.positions.leviersActives.keys()){
			if ((!this.positions.leviers.exists(key) && this.positions.leviersActives[key]) || (this.positions.leviers.exists(key) && !this.positions.leviersActives[key]))
				return false;
		}
		return true;
	}
}