package landes.actions;

import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;

class OpenColorPortal implements IAction {

  public var name(default, null): String = Obfu.raw("openColorPortal");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

	public function run(ctx: IActionContext): Bool {
		var game = ctx.getGame();
		var x: Null<Float> = ctx.getOptFloat(Obfu.raw("x")).toNullable();
		var y: Null<Float> = ctx.getOptFloat(Obfu.raw("y")).toNullable();
		var pid: Null<Int> = ctx.getOptInt(Obfu.raw("pid")).toNullable();
		if (game.portalMcList[pid] != null) {
			return false;
		}
		game.world.scriptEngine.insertPortal(x, y, pid);
		var cx = game.root.Entity.x_ctr(game.flipCoordCase(x));
		var cy = game.root.Entity.y_ctr(y) - game.root.Data.CASE_HEIGHT * 0.5;
		var sprite = game.depthMan.attach("hammer_portal", game.root.Data.DP_SPRITE_BACK_LAYER);
			sprite._x = cx;
			sprite._y = cy;
			sprite.filters = [ColorMatrix.fromHsv(Std.random(360),1, 1).toFilter()];
		game.fxMan.attachExplosion(cx, cy, 30);
		game.fxMan.inGameParticles(game.root.Data.PARTICLE_PORTAL, cx, cy, 5);
		game.fxMan.attachShine(cx, cy);
		game.portalMcList[pid] = {x: cx, y: cy, mc: sprite, cpt: 0};
		return true;
	}
}