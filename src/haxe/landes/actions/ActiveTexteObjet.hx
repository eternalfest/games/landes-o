package landes.actions;

import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class ActiveTexteObjet implements IAction {

	public var name(default, null): String = Obfu.raw("activeTexteObjet");
	public var isVerbose(default, null): Bool = false;

	public function new() {}

	public function run(ctx: IActionContext): Bool {
		var game = ctx.getGame();
		game.root.Data.CONVERT_DIAMANT = 17;
		return true;
	}
}