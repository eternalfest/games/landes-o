package landes.actions;

import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class ItemAll implements IAction {

	public var name(default, null): String = Obfu.raw("itemAll");
	public var isVerbose(default, null): Bool = false;

	public function new() {}

	public function run(ctx: IActionContext): Bool {
		var game = ctx.getGame();
		var i: Null<Int> = ctx.getOptInt(Obfu.raw("i")).toNullable();
		var si: Null<Int> = ctx.getOptInt(Obfu.raw("si")).toNullable();
		game.root.Data.CONVERT_DIAMANT = i;
		game.world.scriptEngine.script.toString();
		game.world.scriptEngine.safeMode();
		game.killPop();
		var v5 = 0;
		for (v6 in 0...game.root.Data.LEVEL_HEIGHT) {
		  for (v7 in 0...game.root.Data.LEVEL_WIDTH) {
			if (game.world.checkFlag({x: v7, y: v6}, game.root.Data.IA_TILE_TOP)) {
			  var v8 = v5 * 2;
			  if (v5 < 4) {
				v8 = 1;
			  }
			  game.world.scriptEngine.insertScoreItem(i, si, game.flipCoordCase(v7), v6, v8, null, true, false);
			  ++v5;
			}
		  }
		}
		game.perfectItemCpt = v5;
		return true;
	}
}