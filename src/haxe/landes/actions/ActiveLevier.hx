package landes.actions;

import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

import color_matrix.ColorMatrix;

import hf.mode.GameMode;
import landes.dimensions.PositionDims;
import patchman.HostModuleLoader;

class ActiveLevier implements IAction {

	public var name(default, null): String = Obfu.raw("activeLevier");
	public var isVerbose(default, null): Bool = false;
	public var positions : PositionDims;
	public var loader : HostModuleLoader;

	public function new(pos : PositionDims) {
		this.positions = pos;
	}

	public function run(ctx: IActionContext): Bool {
		var game = ctx.getGame();
		var id: Null<Int> = ctx.getOptInt(Obfu.raw("id")).toNullable();
		this.positions.leviersActives.set(id, true);
		return true;
	}
}