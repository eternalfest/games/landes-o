package landes;

import merlin.IAction;
import merlin.IActionContext;
import etwin.ds.WeakMap;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.ds.FrozenArray;
import etwin.Obfu;

import patchman.module.Run;
import patchman.HostModuleLoader;

import landes.actions.OpenColorPortal;
import landes.actions.OpenColorPortalDim;
import landes.actions.SetWeapon;
import landes.actions.Ordre;
import landes.actions.ActiveLevier;
import landes.actions.BonsLeviers;
import landes.actions.EstBonLevier;
import landes.actions.ItemAll;
import landes.actions.RemoveItemAll;

import landes.dimensions.PositionDims;

import hf.entity.Player;
import hf.Hf;
import hf.FxManager;
import hf.entity.PlayerController;

import ice.Ice;

import landes.spec.SpecNinja;
import landes.spec.SpecPerle;
import landes.spec.SpecPoisson;
import landes.spec.SpecAnneau;
import landes.spec.SpecJugement;
import landes.spec.SpecMalediction;
import landes.spec.SpecCostume;
import landes.spec.SpecMedaillon;
import landes.spec.SpecSakura;
import landes.spec.SpecMissingno;
import landes.spec.SpecCadeau;

import atlas.PropBridge;
import atlas.Property;
import atlas.PropTypes;
import atlas.Atlas;

import landes.Big_O;

import quete.Quetes;

import vault.ISpec;

@:build(patchman.Build.di())
class Landes {
	
	@:diExport
	public var actions(default, null): FrozenArray<IAction>;
	
	@:diExport
	public var EFFET_NINJA(default, null): ISpec;
	@:diExport
	public var EFFET_PERLE(default, null): ISpec;
	@:diExport
	public var EFFET_POISSON(default, null): ISpec;
	@:diExport
	public var EFFET_ANNEAU(default, null): ISpec;
	@:diExport
	public var EFFET_JUGEMENT(default, null): ISpec;
	@:diExport
	public var EFFET_MALEDICTION(default, null): ISpec;
	@:diExport
	public var EFFET_COSTUME(default, null): ISpec;
	@:diExport
	public var EFFET_MEDAILLON(default, null): ISpec;
	@:diExport
	public var EFFET_SAKURA(default, null): ISpec;
	@:diExport
	public var EFFET_MISSINGNO(default, null): ISpec;
	@:diExport
	public var EFFET_CADEAU(default, null): ISpec;
	
	@:diExport
	public var specItemForce(default, null): PropBridge;
	
	@:diExport
	public var registerBig_O(default, null): IPatch = Ref
	  .auto(hf.mode.GameMode.attachBad)
	  .wrap(function(hf, self, id, x, y, old): hf.entity.Bad {
		if (id == Big_O.ID) {
			self.badCount++;
			return Big_O.attach(self, x, y);
		}else {
			return old(self, id, x, y);
		}
	});
	
	@:diExport
	public var PATCH_O_RESPAWN(default, null): IPatch = Ref
		.auto(hf.entity.Player.resurrect)
		.after(function(hf, self):Void {
			for (fruit in self.game.getBadList())
			{
				if ((cast fruit).estAttaqueGrise){
					self.changeWeapon(self.game.root.Data.WEAPON_NONE);
					self.game.fxMan.attachFx(self.x, self.y - self.game.root.Data.CASE_HEIGHT * 0.5, "hammer_fx_shine");
				}
			};
	});
	
	@:diExport
	public var invocation(default, null): IPatch = Ref
		.auto(hf.entity.bomb.bad.BossBomb.onExplode)
		.wrap(function(hf: hf.Hf, self: hf.entity.bomb.bad.BossBomb, old): Void {
                Reflect.callMethod(self, untyped hf.entity.bomb.BadBomb.prototype.update, []);

                var fruitArray: Array<Dynamic> = [
                    hf.entity.bad.walker.Cerise,
                    hf.entity.bad.walker.Orange,
                    hf.entity.bad.walker.Pomme,
                    hf.entity.bad.walker.Banane,
                    hf.entity.bad.walker.Citron,
                    hf.entity.bad.walker.Bombe,
                    hf.entity.bad.walker.Poire,
                    hf.entity.bad.walker.Fraise,
                    hf.entity.bad.walker.Litchi,
                    hf.entity.bad.walker.LitchiWeak,
                    hf.entity.bad.walker.Abricot,
                    hf.entity.bad.walker.Kiwi,
                    hf.entity.bad.flyer.Baleine,
                    hf.entity.bad.walker.Ananas,
                    hf.entity.bad.walker.Framboise
                ];
                var fruitWeights = [0, 50, 0, 0, 50, 0, 50, 1, 0, 0, 0, 0, 0, 1, 1];
                var fruitWeightSum = 0;
                for (i in 0...fruitWeights.length) {
                    fruitWeightSum += fruitWeights[i];
                }
                var r = Std.random(fruitWeightSum);
                var cumulativeWeight = 0;
                var pickIndex = 0;
                for (i in 0...fruitWeights.length) {
                    cumulativeWeight += fruitWeights[i];
                    if (cumulativeWeight > r) {
                        pickIndex = i;
                        break;
                    }
                }

                var v3 = fruitArray[pickIndex].attach(self.game, self.x - self.game.root.Data.CASE_WIDTH * 0.5, self.y - self.game.root.Data.CASE_HEIGHT * 0.5);
                var v4: hf.entity.boss.Tuberculoz = cast self.game.getOne(self.game.root.Data.BOSS);
                if (v4.lives <= 70) {
                    v3.angerMore();
                }
                if (v4.lives <= 50) {
                    v3.angerMore();
                }
                v3.moveUp(10);
                v3.knock(self.game.root.Data.SECOND);
                untyped {
                    v3.dropReward = null;
                }
                self.playAnim(self.game.root.Data.ANIM_BOMB_EXPLODE);
            });
	
	@:diExport
	public var remplace_carte(default, null): IPatch = Ref
	  .auto(hf.mode.GameMode.onMap)
	  .wrap(function(hf, self, old): Void {
			self.onPause();
	});
	
	@:diExport
	public var patches(default, null) : IPatch;
	
	public function new(ordre : Ordre, run: patchman.module.Run, loader : HostModuleLoader, quete : Quetes): Void {
		var position = new PositionDims(run);
		patchman.DebugConsole.log(position.niveaux);
		patchman.DebugConsole.log(position.leviers);
		
		//Objets découvertes
		EFFET_NINJA = new SpecNinja();
		EFFET_PERLE = new SpecPerle();
		EFFET_POISSON = new SpecPoisson();
		EFFET_ANNEAU = new SpecAnneau();
		EFFET_JUGEMENT = new SpecJugement();
		EFFET_MALEDICTION = new SpecMalediction();
		EFFET_COSTUME = new SpecCostume();
		EFFET_MEDAILLON = new SpecMedaillon();
		EFFET_SAKURA = new SpecSakura(quete);
		EFFET_MISSINGNO = new SpecMissingno();
		EFFET_CADEAU = new SpecCadeau();
		
		specItemForce = SpecAnneau.ITEM_FORCE.bridge(Obfu.raw("specItemForce"), PropTypes.INT);
		
		patches = new PatchList([
			//Patchs actions
			ordre.modificationNiveauEntree,
			ordre.modificationCristalGeant,
			position.PATCH_GESTION_DIMS,
			invocation,
			//Patchs objets
			(cast EFFET_ANNEAU).PATCH,
			(cast EFFET_MALEDICTION).PATCH_getScore,
			(cast EFFET_MALEDICTION).PATCH_getScoreHidden,
			(cast EFFET_MALEDICTION).PATCH_aura,
			(cast EFFET_COSTUME).PATCH_attack,
			(cast EFFET_COSTUME).PATCH_kickBomb,
			(cast EFFET_MEDAILLON).PATCH_pause
		]);
		
        actions = FrozenArray.of(
			new OpenColorPortal(),
			new SetWeapon(),
			new OpenColorPortalDim(loader, position),
			new ActiveLevier(position),
			new BonsLeviers(position),
			new EstBonLevier(position),
			new ItemAll(),
			new RemoveItemAll(),
			ordre.levelOrdre
		);
	}
}