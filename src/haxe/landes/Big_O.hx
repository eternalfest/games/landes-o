package landes;

import merlin.value.MerlinFloat;
import merlin.value.MerlinValue;
import merlin.Merlin;

import etwin.ds.WeakMap;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.ds.FrozenArray;
import etwin.Obfu;

import landes.dimensions.PositionDims;

import hf.entity.Player;
import hf.Hf;
import hf.FxManager;
import hf.entity.PlayerController;
import hf.entity.Shoot;
import hf.mode.GameMode;

import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;

import landes.PepinBig_O;

@:hfTemplate
class Big_O extends hf.entity.bad.walker.Orange {

	public static var ID(default, never)	: Int = 102;
	public static var GROSSEUR 				: Int = 290;
	public static var FILTRE				: ColorMatrixFilter = ColorMatrix.fromHsv(180, 0.85, 0.8).toFilter();
	public static var ETAT_PREPARATION 		: Int = 0;
	public static var ETAT_ATTAQUE 			: Int = 1;
	public static var ETAT_ATTENTE 			: Int = 2;
	public static var INVOCATIONS_MAX		: Int = 5;
	public static var INVOCATIONS_DELAI		: Float = 10;
	public static var INVOCATIONS_PREPA		: Int = 1;
	public static var BOULES_FEU_DELAI		: Float = 1;
	public static var PROBABILITE_SAUTER	: Float = 1;
	public static var PROBABILITE_PRESSAGE	: Float = 1;
	public static var VIE					: Int = 100;
	
	public static var FOND_GRIS 			: Int = 19;
	public static var FOND_NOIR 			:  Int = 20;
	public static var FOND_BLANC 			:  Int = 21;
	public static var FOND_ROUGE 			:  Int = 22;
	public static var FOND_JAUNE 			:  Int = 23;
	public static var FOND_VERT 			:  	Int = 24;
	public static var FOND_BLEU 			:  Int = 25;
	
	private var fl_degonfle 				: Bool = false;
	private var coeffScale 					: Int = GROSSEUR;
	private var fl_saute 					: Bool = false;
	private var fl_invocateur				: Bool = false;
	private var fl_jaune					: Bool = false;
	private var thermometre					: Dynamic;
	private var vie							: Float = VIE;
	
	private var attaque_fonction 			= function() : Void{};
	private var attaque_fond 				: Int = 0;
	private var attaque_temps				: Float = 0;
	private var attaque_temps_attaque 		: Float = 0;
	private var attaque_temps_attente 		: Float = 5*32;
	private var attaque_temps_preparation	: Float = 0;
	private var attaque_etat				: Int = ETAT_ATTENTE;
	private var invocation_temps			: Float = 3*32;
	private var jaune_temps					: Float = 0;
	
	private var filtreDegonfle				: Null<ColorMatrixFilter>;
	private var filtreGlow					: Null<etwin.flash.filters.GlowFilter>;
	
	private var georges						: Null<hf.entity.bad.FireBall>;
	
	private var estBigO						: Bool = true;
	private var estAttaqueGrise				: Bool = false;
		
	public function new(): Void {
		super(null);
		this.fl_freeze = false;
		this.fl_moveable = false;
	}
	
	public static function attach(g: GameMode, x: Float, y: Float): Big_O {
		var hf : Hf = g.root;
		hf.Std.registerClass(Obfu.raw("boss_big_o"), Big_O.getClass(hf));
		var bad : Big_O = cast g.depthMan.attach(Obfu.raw("boss_big_o"), hf.Data.DP_BADS);
			bad.initBad(g, x, y);
			bad.scale(Big_O.GROSSEUR);
			bad.setFall(75);
			bad.thermometre = g.depthMan.attach("hammer_interf_boss_bar", hf.Data.DP_INTERF);
			bad.thermometre._rotation = -90;
			bad.thermometre._x = 0;
			bad.thermometre._y = hf.Data.GAME_HEIGHT * 0.5;
		return bad;
	}
	
	public override function freeze(timer: Float) : Void{
		this.tirerPepins(6);
	};
	
	public function invocation(): Void {
		var v6 = this.game.root.entity.bomb.bad.BossBomb.attach(this.game, 0, 0);
			v6.moveTo(Std.random(Math.round(this.game.root.Data.GAME_WIDTH * 0.8)) + this.game.root.Data.GAME_WIDTH * 0.1, Std.random(150) + 100);
			this.game.fxMan.attachFx(v6.x, v6.y - this.game.root.Data.CASE_HEIGHT * 0.5, "hammer_fx_pop");
	}
	
	public function degonfle() : Void{
		this.fl_degonfle = true;
		this.coeffScale = 45;
		this.scale(this.coeffScale);
		this.filtreDegonfle = Big_O.FILTRE;
		this.filters = [this.filtreDegonfle, this.filtreGlow];
	};
	
	public function sauter(n : Int) : Void{
		this.dy = -20 * n;
		this.fl_saute = true;
	};
	
	public function decideSauter() : Void{
		if (this.fl_stable && Std.random(1000) < Big_O.PROBABILITE_SAUTER){
			this.sauter(Std.random(3)+1);
		}
	};
	public function decidePressage() : Void{
		if (this.fl_stable && Std.random(1000) < Big_O.PROBABILITE_PRESSAGE){
			this.freeze(0);
		}
	};
	
	public function tirerPepins(n : Int) : Void{
		if (n > 20){
			n = 20;
		}
		var angleAleatoire : Float = Std.random(360);
		for (i in (-n) ... (n + 1)){
			var pepin = PepinBig_O.attach(this.game, this.x, this.y);
			var angle = angleAleatoire + 360 / n * i;
				pepin.moveToAng(angle, pepin.shootSpeed);
		};
		this.degonfle();
	};
	
	override public function killHit(dx: Null<Float>): Void {}
	override public function knock(timer: Float): Void {}
	
	override public function hit(e: hf.Entity): Void {
		var data = this.game.root.Data;
		if ((e.types & data.BAD) > 0 && !this.fl_degonfle) {
			var fruit : hf.entity.Bad = cast e;
			var vitesse = fruit.evaluateSpeed();
			if (fruit.fl_freeze && vitesse >= data.ICE_HIT_MIN_SPEED) {
				this.viePerdue(10);
				fruit.killHit(null);
				this.attaqueStop();
				this.attaque_etat = Big_O.ETAT_ATTENTE;
				this.attaque_temps = this.game.root.Data.SECOND * 3;
			}else{
				if (!Std.is(fruit, this.game.root.entity.bad.FireBall)){
					this.viePerdue(-1);
					fruit.burn();
				}
			}
		}
		else if((e.types & data.BAD) == 0){
			super.hit(e);
		}
	}
	
	
	public override function update() : Void{
		this.decideSauter();
		this.decidePressage();
		this.attaque_temps -= this.game.root.Timer.tmod;
		this.invocation_temps -= this.game.root.Timer.tmod;
		this.jaune_temps -= this.game.root.Timer.tmod;
		this.game.huTimer = 0;
		super.update();
		if (this.fl_degonfle && this.coeffScale < Big_O.GROSSEUR){
			this.coeffScale += 1;
			this.scale(this.coeffScale);
			if (this.coeffScale == Big_O.GROSSEUR){
				this.fl_degonfle = false;
				this.filtreDegonfle = null;
				this.filters = [this.filtreDegonfle, this.filtreGlow];
			}
		}
		if (this.invocation_temps < 0 && this.fl_invocateur){
			this.invocation();
			this.unstick();
			this.invocation_temps = this.game.root.Data.SECOND * Big_O.INVOCATIONS_DELAI;
			this.fl_invocateur = false;
		}
		else if (this.invocation_temps < this.game.root.Data.SECOND * Big_O.INVOCATIONS_PREPA && !this.fl_invocateur)
		{
			var nbInvocs = this.game.getBadList().length - 1;
			if (nbInvocs < Big_O.INVOCATIONS_MAX){
				var v5 = this.game.depthMan.attach("curse",  this.game.root.Data.DP_FX);
					v5.gotoAndStop("" +  this.game.root.Data.CURSE_TAUNT);
					v5._xscale = 200;
					v5._yscale = 200;
				this.stick(v5, 0, - this.game.root.Data.CASE_HEIGHT * 4);
				this.fl_invocateur = true;
			}
		}
		if(this.fl_jaune && this.jaune_temps < 0){
			var missile = this.game.root.entity.shoot.FireBall.attach(this.game, this.x + 20, this.y - 20);
				missile.moveToTarget(this.game.getOne(this.game.root.Data.PLAYER), this.game.root.entity.boss.Tuberculoz.FIREBALL_SPEED * 1.5);
			this.jaune_temps = Big_O.BOULES_FEU_DELAI * this.game.root.Data.SECOND;
		}
		switch (this.attaque_etat) {
			case Big_O.ETAT_ATTENTE:
				if (this.attaque_temps < 0){
					var alea = Std.random(7);
					switch (alea){
						case 0 : prepareAttaque(this.attaqueGrise, 2, 15, Big_O.FOND_GRIS);
						case 1 : prepareAttaque(this.attaqueNoire, 2, 20, Big_O.FOND_NOIR);
						case 2 : prepareAttaque(this.attaqueBlanche, 2, 10, Big_O.FOND_BLANC);
						case 3 : prepareAttaque(this.attaqueRouge, 2, 25, Big_O.FOND_ROUGE);
						case 4 : prepareAttaque(this.attaqueJaune, 2, 20, Big_O.FOND_JAUNE);
						case 5 : prepareAttaque(this.attaqueVerte, 2, 15, Big_O.FOND_VERT);
						case 6 : prepareAttaque(this.attaqueBleue, 2, 15, Big_O.FOND_BLEU);
					}
					this.attaque_etat = Big_O.ETAT_PREPARATION;
				}
			case Big_O.ETAT_ATTAQUE:
				if (this.attaque_temps < 0){
					this.attaqueStop();
					this.attaque_temps = this.game.root.Data.SECOND * 3;
					this.attaque_etat = Big_O.ETAT_ATTENTE;
				}
			case Big_O.ETAT_PREPARATION:
				if (this.attaque_temps < 0){
					this.attaque_fonction();
					this.attaque_temps = this.attaque_temps_attaque;
					this.attaque_etat = Big_O.ETAT_ATTAQUE;
					this.game.fxMan.attachBg(this.attaque_fond, 0, 9999);
				}
			case _:
		}
	};
	
	public function prepareAttaque(fonction_attaque, preparation, attaque, fond) : Void{
		this.attaque_temps_preparation 	= preparation * this.game.root.Data.SECOND;
		this.attaque_temps_attaque 		= attaque * this.game.root.Data.SECOND;
		this.attaque_temps 				= this.attaque_temps_preparation;
		this.attaque_fonction 			= fonction_attaque;
		this.attaque_fond 				= fond;
	};
	
	public function attaqueStop() : Void{
		this.filtreGlow = null;
		this.estAttaqueGrise = false;
		this.filters = [this.filtreDegonfle, this.filtreGlow];
		var joueurs = this.game.getPlayerList();
		for (joueur in joueurs){
			if (joueur.currentWeapon == this.game.root.Data.WEAPON_NONE){
				joueur.changeWeapon(this.game.root.Data.WEAPON_B_CLASSIC);
				this.game.fxMan.attachFx(joueur.x, joueur.y - this.game.root.Data.CASE_HEIGHT * 0.5, "hammer_fx_shine");
			}
			else if (joueur.alpha == 0){
				joueur.alpha = 100;
				this.game.fxMan.attachFx(joueur.x, joueur.y - this.game.root.Data.CASE_HEIGHT * 0.5, "hammer_fx_shine");
			}
		}
		var fruits = this.game.getBadClearList();
		for (fruit in fruits){
			if (fruit.alpha != 100){
				fruit.alpha = 100;
				this.game.fxMan.attachFx(fruit.x, fruit.y - this.game.root.Data.CASE_HEIGHT * 0.5, "hammer_fx_shine");
			}
		}
		this.game.forcedDarkness = 0;
		this.game.updateDarkness();
		this.game.setDynamicVar(Obfu.raw("ICE"), null);
		this.fl_jaune = false;
		this.game.flipX(false);
        this.game.flipY(false);
		this.game.fxMan.clearBg();
	};
	
	public function filtrer(color : Int) : Void{
		this.filtreGlow = new etwin.flash.filters.GlowFilter();
		this.filtreGlow.color = color;
		this.filtreGlow.alpha = .9;
		this.filtreGlow.strength = 90;
		this.filtreGlow.blurX = 8;
		this.filtreGlow.blurY = 8;
		this.filters = [this.filtreDegonfle, this.filtreGlow];
	};
	
	public function attaqueGrise() : Void {
		this.filtrer(8421504);
		var joueurs = this.game.getPlayerList();
		this.estAttaqueGrise = true;
		for (joueur in joueurs){
			joueur.changeWeapon(this.game.root.Data.WEAPON_NONE);
			this.game.fxMan.attachFx(joueur.x, joueur.y - this.game.root.Data.CASE_HEIGHT * 0.5, "hammer_fx_shine");
		}
	};
	public function attaqueNoire() : Void {
		this.filtrer(0);
		this.game.forcedDarkness = 200;
		this.game.updateDarkness();
	};
	public function attaqueBleue() : Void {
		this.filtrer(255);
		this.game.setDynamicVar(Obfu.raw("ICE"), 1);
	};
	public function attaqueBlanche() : Void {
		this.filtrer(16777215);
		var joueurs = this.game.getPlayerList();
		for (joueur in joueurs){
			joueur.minAlpha = 0;
			joueur.alpha = 0;
			this.game.fxMan.attachFx(joueur.x, joueur.y - this.game.root.Data.CASE_HEIGHT * 0.5, "hammer_fx_shine");
		}
		var fruits = this.game.getBadClearList();
		for (fruit in fruits){
			fruit.alpha = 20;
			this.game.fxMan.attachFx(fruit.x, fruit.y - this.game.root.Data.CASE_HEIGHT * 0.5, "hammer_fx_shine");
		}
	};
	public function attaqueRouge() : Void {
		this.filtrer(16711680);
		var joueurs = this.game.getPlayerList();
		for (joueur in joueurs) {
			this.georges = this.game.root.entity.bad.FireBall.attach(this.game, joueur);
				georges.setLifeTimer(this.game.root.Data.SECOND * 40);
		}
	};
	public function attaqueJaune() : Void {
		this.filtrer(16776960);
		this.fl_jaune = true;
	};
	public function attaqueVerte() : Void {
		this.filtrer(65280);
		if (Std.random(2) == 0){
			this.game.flipX(true);
		}else{
			this.game.flipY(true);
		}
	};
	
	public function updateThermometre(): Void {
		this.thermometre.barFade._xscale = this.thermometre.bar._xscale;
		this.thermometre.barFade.gotoAndPlay("1");
		this.thermometre.bar._xscale = (this.vie / Big_O.VIE) * 100;
	}
	
	public function viePerdue(n : Float): Void {
		this.vie -= n;
		if (n > 5){
			this.georges.destroy();
			this.georges = null;
			this.freeze(0);
			this.game.shake(this.game.root.Data.SECOND * 0.5, 5);
			var joueurs = this.game.getPlayerList();
			for (joueur in joueurs){
				if (joueur.fl_stable) {
					if (joueur.fl_shield) {
						joueur.dy = -8;
					} else {
						joueur.knock(this.game.root.Data.PLAYER_KNOCK_DURATION*.75);
					}
				}
			}
			var fruits = this.game.getBadList();
			for (fruit in fruits){
				fruit.knock(this.game.root.Data.PLAYER_KNOCK_DURATION);
			}
		}
		this.updateThermometre();
		if(this.vie < 0){
			var fruits = this.game.getBadList();
			for (fruit in fruits){
				if(!(Std.is(fruit, Big_O)))
					fruit.killHit(null);
			}
			this.game.destroyList(this.game.root.Data.BAD_BOMB);
			this.game.destroyList(this.game.root.Data.SHOOT);
			this.playAnim(this.game.root.Data.ANIM_BAD_DIE);
			this.fl_kill = true;
			this.fl_hitGround = false;
			this.fl_strictGravity = true;
			this.fl_hitGround= false;
			this.fl_hitCeil= false;
			this.fl_hitWall= false;
			this.thermometre.removeMovieClip();
			this.dy= -8;
		}
	}
}
