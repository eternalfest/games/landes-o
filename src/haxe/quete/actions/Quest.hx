package quete.actions;

import merlin.IAction;
import merlin.IActionContext;
import patchman.HostModuleLoader;
import etwin.Obfu;

import hf.GameManager;
import quete.Quetes;

class Quest implements IAction {

	public var name(default, null): String = Obfu.raw("quest");
	public var isVerbose(default, null): Bool = true;
	public var quete : Quetes;
    
    public function new(quetes : Quetes) {
		this.quete = quetes;
	}

	 public function run(ctx: IActionContext): Bool {
        var game = ctx.getGame();
        var id: Null<Int> = ctx.getOptInt(Obfu.raw("id")).toNullable();// id de la quete du data, et non la famille !
        return this.quete.estQueteCompletee(id);
    }
}