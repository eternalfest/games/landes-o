package quete;

import patchman.IPatch;
import patchman.Ref;
import etwin.ds.FrozenArray;
import hf.Hf;

import keyboard.Key;
import keyboard.KeyCode;
import hf.entity.PlayerController;
import etwin.Obfu;
import patchman.HostModuleLoader;

@:build(patchman.Build.di())
class QueteDash {

	public static var TEMPS_DASH = 1;
	public static var LONGUEUR_DASH = 4;

	public var listeTemps = [0.0,0.0];
	@:diExport
	public var queteDash(default, null): IPatch;
	
	public function dash(hf: Hf, self: PlayerController) : Bool{
		if(self.player.fl_stable && listeTemps[self.player.skin - 1] <= 0){
			var fruits = self.game.getBadList();
			var cx = self.player.cx;
			var cy = self.player.cy;
			var d = false;
			for (i in 0...QueteDash.LONGUEUR_DASH){
				var type = self.game.world.getCase({x: cx + (i+1)*self.player.dir, y: cy});
				if (type != hf.Data.WALL && type != hf.Data.GROUND){
					if(!d){
						listeTemps[self.player.skin - 1] = hf.Data.SECOND * QueteDash.TEMPS_DASH;
						var spriteDash = self.game.fxMan.attachFx(self.player.x-20, self.player.y-30, Obfu.raw("dash"));
						if(self.player.dir > 0)
							spriteDash.mc._x += -30;
						else{
							spriteDash.mc._x -= 4*20;
						}
						d = true;
					}
					
					self.player.x += self.player.dir*20;
					self.player.updateCoords();
					for (fruit in fruits){
						if (fruit.hitBound(self.player)){
							if (fruit.isType(hf.Data.BAD_CLEAR)){
								if(Std.is(fruit, hf.entity.bad.walker.Bombe)){
									(cast fruit).trigger();
								}
								if(fruit.isHealthy())
									fruit.knock(hf.Data.KNOCK_DURATION/2);
							}
							else if(Std.is(fruit, hf.entity.bad.Spear) && !self.player.fl_shield){
								self.player.killHit(0);
								return d;
							}
							else if(Std.is(fruit, hf.entity.bad.ww.Saw)){
								(cast fruit).stun();
							}
						}
					}
					if (type < 0){
						self.player.infix();
						if(type == hf.Data.FIELD_TELEPORT)
							break;
					}
					
				}
				else{
					return d;
				}
				self.player.y--;
			}
			return d;
		}
		return false;
	}

	public function new(quete : Quetes) {
		this.queteDash = Ref.auto(PlayerController.getControls)
			.after(function(hf: Hf,self : PlayerController) : Void{
				for (i in 0...2){
					this.listeTemps[i] -= hf.Timer.tmod;
				}
				if(Key.isDown(KeyCode.X) && !self.keyLocks[0x58] && quete.estQueteCompletee(13)){
					if(dash(hf, self)){
						self.player.checkHits();
					}
					
					self.lockKey(0x58);
				}
			});
		}

}

