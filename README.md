# La Bambouseraie

Clone with HTTPS:
```shell
git clone https://gitlab.com/eternalfest/games/bambouseraie.git
```

Clone with SSH:
```shell
git clone git@gitlab.com:eternalfest/games/bambouseraie.git
```
